<!--- source qa rewrite begin -->
### QA
[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)
[![CircleCI](https://circleci.com/gh/vidaxl-com/cowlog/tree/master.svg?style=svg)](https://circleci.com/gh/vidaxl-com/cowlog/tree/master)
[![Test Coverage](https://api.codeclimate.com/v1/badges/d3fce811aecbe5c73ffb/test_coverage)](https://codeclimate.com/github/vidaxl-com/cowlog/test_coverage)
[![Maintainability](https://api.codeclimate.com/v1/badges/d3fce811aecbe5c73ffb/maintainability)](https://codeclimate.com/github/vidaxl-com/cowlog/maintainability)
<!--- source qa rewrite end -->
[![Known Vulnerabilities](https://snyk.io/test/github/vidaxl-com/cowlog/badge.svg?targetFile=package.json)](https://snyk.io/test/github/vidaxl-com/cowlog?targetFile=package.json)
Since 2008NOV20[![HitCount](http://hits.dwyl.com/vidaxl.com/cowlog.svg)](http://hits.dwyl.com/{username}/{project-name})
<!--- 
[![Known Vulnerabilities](https://snyk.io/test/github/vidaxl-com/cowlog/badge.svg?targetFile=package.json)](https://snyk.io/test/github/vidaxl-com/cowlog?targetFile=package.json)
[![FOSSA Status](https://app.fossa.io/api/projects/git%2Bgithub.com%2Fvidaxl-com%2Fcowlog.svg?type=shield)](https://app.fossa.io/projects/git%2Bgithub.com%2Fvidaxl-com%2Fcowlog?ref=badge_shield)
[![Greenkeeper badge](https://badges.greenkeeper.io/vidaxl-com/cowlog.svg)](https://greenkeeper.io/)
-->



#Where are you?

This project is a monorepo, giving home for multiple projects depending on each 
other. We use [lerna](https://github.com/lerna/lerna) to manage the releases 
and the packages efficiently. Each of the project it hosts usable meant ot be
**useful** in their area, are listed below.

# What projects belong to here?

 - **[cowlog](https://github.com/vidaxl-com/cowlog/tree/master/packages/cowlog)** Development time logging for NodeJs developers
 - [directory-fixture-provider](https://github.com/vidaxl-com/cowlog/tree/master/packages/directory-fixture-provider) 
Provides directories for testing. 
 - [generic-text-linker](https://github.com/vidaxl-com/cowlog/tree/master/packages/generic-text-linker) 
Generic text linker for NodeJs. 
 - **[dsl-framework](https://github.com/vidaxl-com/cowlog/tree/master/packages/dsl-framework)**
Currying to create DSLs 

# Motivation
Our aim is to provide tools that has not been released for developers, 
more productive. The original project called cowlog. All the tools here are
developed to remove the repetitive soul crushing tasks from you and be able
to focus to your business needs. 
